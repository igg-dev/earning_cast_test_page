class MyButton extends React.Component {
  
  constructor(props) {
    super(props);
   
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(event){
    if(!this.props.disable){
      return null
    }
    this.props.onClick()
  }

  render() {
      var {
        onClick,
        ...others  
      } = this.props;
      let disableClass, activeClass; 
      disableClass = this.props.disable ?  '' : 'disabled' 
      activeClass = this.props.activate ? 'active' : '' 
    return ( 
      <button type="button" className={`btn btn-default ${disableClass} ${activeClass}`} onClick={this.handleClick} {...others}>
                        {this.props.children}
        </button>
      )
  }
}
