var Transcription = function(cfg) {
    var index = 0;
    var list = [];

    this.add = function(text, isFinal) {
        list[index] = text;
        if (isFinal) {
            index++;
        }
    }

    this.toString = function() {
        return list.join('. ');
    }
};

class ControllerElements extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      servers: ['wss://staging.earningscast.com:8888'],
      record: false,
      fileButton: false, 
      streamButton: false,
      handleFileButton: false,
      audio: '', 
      fileInfo: ''
    };

    this.toggleMic = this.toggleMic.bind(this)
    this.toggleStream = this.toggleStream.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleStream = this.handleStream.bind(this)
    this.updateTranscript = this.updateTranscript.bind(this)
    this.updatePartialTranscript = this.updatePartialTranscript.bind(this)
    this.toggleFile = this.toggleFile.bind(this)
    this.handleFile = this.handleFile.bind(this)
    this.sendFile = this.sendFile.bind(this)
    this.sendStream = this.sendStream.bind(this)
    this.playFile = this.playFile.bind(this)
  }

  componentDidMount() {

    if (!window.AudioContext || !navigator.mediaDevices.getUserMedia) {
        $('#textBrowser').removeClass('hidden');
    }
  }


  updateTranscript(hypos){
    var tr = this.state.transcript;
    tr.add(hypos[0].transcript, true);
    this.setState({transcript: tr})
  }

  updatePartialTranscript(hypos){
    var tr = this.state.transcript;
    tr.add(hypos[0].transcript, false);
    this.setState({transcript: tr})
  }
 
  toggleMic() {
      this.setState((prevState, props)=>({
        record: !prevState.record,
        micButton: !prevState.micButton
      }))
  }

  toggleStream(){
    this.setState((prevState, props)=>({
      stream: !prevState.stream,
      streamButton: !prevState.streamButton
    }))
  }

  handleChange(event) {
    if($(event.target).prop("checked")){
      this.setState((prevState)=>({
        servers: prevState.servers.concat(['wss://staging.earningscast.com:'+$(event.target).data("port")])
      }));
    } else {
      var serverz = this.state.servers
      var index = serverz.indexOf('wss://staging.earningscast.com:'+$(event.target).data("port"));
      serverz.splice(index, 1);
      this.setState({servers: serverz});
    }
  }

  toggleFile(){
    this.setState((prevState, props)=>({
      fileButton: !prevState.fileButton,
      fileInfo: ''
    }))
  }

  handleFile(){
    var file = document.getElementById('audioFile').files[0];
    if (file) {
        if (file.type == 'audio/x-wav' || file.type == 'audio/wav' || file.type == 'audio/mp3') {
          this.setState({
            handleFileButton: true,
            fileInfo: file.name
          })
        } else {
          this.setState({
            handleFileButton: false,
            fileInfo: 'Unsupported format ' + file.type
          })
        }
    }
  } 
  
  handleStream(){

  }

  sendFile(){
    this.setState({audio: $('#audioFile').get(0).files[0]})
  } 
 
  sendStream(){
    var streamUrl=document.getElementById('stream-url').value
    if(streamUrl!=='')
      this.setState({audio: streamUrl})
  }

  playFile(){
    var file = $('#audioFile').get(0).files[0];
    var wavBlob = new Blob([file]);
    var wavURL = window.URL.createObjectURL(wavBlob);
    var audio = new Audio();
    audio.src = wavURL;
    audio.play();
  } 

  render() {
    let disabledMic = this.state.fileButton ? 'disabled' : '';
    let disabledFile = ''
    return (
      <div className="container"> 
          <div className="row">
              <h1>Kaldi Web</h1>
          </div>
          <br/>
          <div className="row">
              <div className="col-sm-12">
                 <MyButton
                 id="buttonStream"
                 onClick={this.toggleStream}
                 disable={!this.state.fileButton}
                 activate={this.state.streamButton}
                 title="Add stream url">Stream</MyButton>
                 <MyButton
                 id="buttonFile"
                 onClick={this.toggleFile}
                 disable={!this.state.micButton}
                 activate={this.state.fileButton}
                 title="Upload an Audio file in wav format">File</MyButton>
        
                 <br/>
                 <br/>
              </div>
                 { this.state.fileButton ?
                   <div className="col-sm-12">
                     <span className="label label-warning" id="infoFile">{this.state.fileInfo}</span>
                     <br/>
                     <label
                     id="buttonBrowse"
                     className="btn btn-default btn-file "
                     title="Browse the file to upload format WAV Mono 16 kHz 16-bit.">Browse
                         <input type="file" id="audioFile" 
                          onChange={this.handleFile}
                          />
                     </label>
                       <MyButton
                       id="buttonSend"
                       onClick={this.sendFile}
                       disable={this.state.handleFileButton}
                       title="Send the file to the server">Send</MyButton>

                       <MyButton
                       id="buttonPlay"
                       disable={this.state.handleFileButton}
                       onClick={this.playFile}
                       title="Listen to the audio">Play</MyButton>

                   </div> :
                   null
                 }
                  { this.state.streamButton ?
                    <div>
                      <input type="text" id="stream-url" 
                          onChange={this.handleStream}
                          />
                       <MyButton
                       id="buttonSend"
                       disable={true}
                       onClick={this.sendStream}
                       title="Send the file to the server">Send</MyButton>
                      
                    </div> :
                    null         
                  }
          
              <div className="col-sm-12">
                {this.state.servers.indexOf('wss://staging.earningscast.com:8888')>-1 ?
                  <label for="TypeOne"><input type="checkbox" checked="checked" data-port="8888" name="TypeOne" className="typeVal" value="0" onChange={this.handleChange} ></input>  Type One</label> :
                  <label for="TypeOne"><input type="checkbox" data-port="8888" name="TypeOne" className="typeVal" value="0" onChange={this.handleChange} ></input>  Type One</label>
                }
                <label for="TypeTwo"><input type="checkbox" data-port="9999" name="TypeTwo" className="typeVal" value="0" onChange={this.handleChange} ></input>  Type Two</label>
              </div>
            </div>
            <div className="row">
              {this.state.servers.map(function (ser, ind) {
                  return(
                    <Textarea audio={this.state.audio} record={this.state.record} typee={ser} key={ind}/>
                  ) 
              }, this)}
            </div>  
      </div>
    )
  }
}



const contentz = (
    <ControllerElements />

);

ReactDOM.render(
  contentz,
  document.getElementById('root')
);