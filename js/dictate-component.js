class DictateComponent extends React.Component {
  
  constructor(props) {
    super(props);
    
    var updateTranscript = this.updateTranscript.bind(this)
    var updatePartialTranscript = this.updatePartialTranscript.bind(this)
    var cfgDictate = {
        onPartialResults : function(hypos) {
            updatePartialTranscript(hypos)
        },
        onResults : function(hypos) {
            updateTranscript(hypos)
        },
        server: this.props.server
    };

    this.state = {
      dictate: new Dictate(cfgDictate)
    };

    // this.handleChange = this.handleChange.bind(this)
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.record && !this.props.record){
      this.state.dictate.record()
      console.log('recording')
    } else if(!nextProps.record && this.props.record){
      this.state.dictate.stop()
      console.log("stopped recording")
    } else if (nextProps.audio!=''){
      this.state.dictate.send(nextProps.audio)
    }
  }
  
  shouldComponentUpdate(nextProps, nextState){
    if(nextProps.record==this.props.record){
      return false
    } else {
      return true
    }
  }

  updateTranscript(hypos){
    this.props.onUpdate(hypos)
  }

  updatePartialTranscript(hypos){
    this.props.onPartialUpdate(hypos)
  }

  render() {
    return (
      null
    )
  }
}
