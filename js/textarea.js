var Transcription = function(cfg) {
    var index = 0;
    var list = [];

    this.add = function(text, isFinal) {
        list[index] = text;
        if (isFinal) {
            index++;
        }
    }

    this.toString = function() {
        return list.join('. ');
    }
};

class Textarea extends React.Component {
  
  constructor(props) {
    super(props);
  
    this.state = {
      transcript: new Transcription()
    };

    this.saveTranscript = this.saveTranscript.bind(this)
    this.clearTranscript = this.clearTranscript.bind(this)
    this.updateTranscript = this.updateTranscript.bind(this)
    this.updatePartialTranscript = this.updatePartialTranscript.bind(this)
  }

  saveTranscript(event){
    var text = this.state.transcript;
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', 'transcription.txt');
    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();
    document.body.removeChild(element);
  } 


  clearTranscript(event){
    var tr = new Transcription()
    this.setState({transcript: tr})
  }
  
  updateTranscript(hypos){

    // this.props.updateTranscript(this.props.transcript, hypos)
    var tr = this.state.transcript;
    tr.add(hypos[0].transcript, true);
    this.setState({transcript: tr})
  }

  updatePartialTranscript(hypos){
 
    // this.props.updateTranscript(this.props.transcript, hypos)
    console.log(hypos[0].transcript)
    var tr = this.state.transcript;
    tr.add(hypos[0].transcript, false);
    this.setState({transcript: tr})
  }

  render() {
    return (
              <div className="col-sm-5">
                  <DictateComponent audio={this.props.audio} record={this.props.record} onUpdate={this.updateTranscript} onPartialUpdate={this.updatePartialTranscript} server={this.props.typee} />
                    <span className="label label-info" >type: {this.props.typee}</span>
                    <textarea className="form-control trans" rows="10" value={this.state.transcript}></textarea>
                    <div className="col-sm-4">
                        <button
                        // type="button"
                        id="buttonSave"
                        className="btn btn-default"
                        title="Save transcript to a local file"
                        onClick={this.saveTranscript} >Save</button>
                        <button
                        // type="button"
                        id="buttonClear"
                        className="btn btn-default"
                        title="Clear the transcription"
                        onClick={this.clearTranscript} >Clear</button>
                    </div>
              </div>
    )
  }
}
